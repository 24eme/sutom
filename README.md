# ORACCEB

Jeu de lettres en ligne (et en français) basé sur Wordle et le regretté SUTOM. Le jeu se trouve à l'adresse https://oracceb.24eme.fr

## Liste des instances

Référencement de toutes les fork/instances de sutom.nocle.fr :

- https://oracceb.24eme.fr

## Déployer son instance

Récupérer le projet

```
git clone https://framagit.org/24eme/sutom.git
```

Récupérer et synchroniser la liste des mots à trouver :

```sh
bash synchronize.sh
```

En attendant de trouver une solution plus ditribuer pour partager la liste de mots, ce script va chercher les mots sur sutom.nocle.fr : https://framagit.org/JonathanMM/sutom/-/issues/56

Ce script `synchronize.sh` va chercher le mot du jour et celui du lendemain, il faut donc le programmer pour l'éxecuter tous les jours, à l'aide d'un crontab.

Générer la liste des mots possibles :

```sh
node utils/nettoyage.js
```

Démarrer le serveur :

```sh
npm start
```

## Contributions

Tout d'abord, merci si vous contribuez :) Pour l'instant, le mieux, c'est de créer un ticket quand vous voyez un bug, ça me permettra de trier et de prioriser tout ce que je dois faire. Comme la base de code n'est pas aussi propre que je voudrais, merci de créer un ticket et d'attendre un retour de ma part ( @JonathanMM ) avant de vous lancer à corps perdu dans le code.

## Développement

### Avec npm

Pour pouvoir travailler en local, il faut commencer par installer ce qu'il faut à node :

```sh
npm i
```

Puis, on lance le serveur :

```sh
npm run start:dev
```

### Avec Docker

Un Dockerfile est disponible pour pouvoir démarrer le site en local sans `npm`.

```sh
docker build --build-arg MODE=development -t sutom .

docker run -it --rm -p 4000:4000 sutom npm run start:dev
```

### Accès au site

Une fois démarré, le site sera dispo sur http://localhost:4000 et le typescript va se recompiler tout seul à chaque modification de fichier.

## Déployer en production

### Avec npm

Pour déployer en production, on installe les dépendances :

```sh
npm install --production
```

Puis on lance le serveur :

```sh
npm start
```

### Avec Docker

On lance Docker en production en créant l'image et en la lançant sans les options particulières pour le mode "development" :

```sh
docker build -t sutom .

docker run -it --rm -p 4000:4000 sutom
```

## Autres infos et remerciements

- Le dictionnaire utilisé est celui de [Grammalecte](https://grammalecte.net/dictionary.php?prj=fr). Merci à GaranceAmarante pour le script.
- Merci à Emmanuel pour m'avoir fourni des mots à trouver.
- Merci à tous les gens qui me remontent des bugs et qui me donnent des idées, ça m'aide beaucoup :)
- Merci à toutes les personnes qui jouent, c'est une belle récompense que vous me donnez.
